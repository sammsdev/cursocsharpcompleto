﻿using System;
using System.Globalization;

namespace Xadrez.ExerciciosIntermediario.Exercicio___Heranca_e_Polimorfismo.Entities
{
    class Product
    {
        public string Name { get; private set; }
        public double Price { get; private set; }

        public Product()
        {
        }

        public Product(string name, double price)
        {
            Name = name;
            Price = price;
        }


        public virtual string PriceTag()
        {
            return Name + " $" + Price.ToString(CultureInfo.InvariantCulture);
        }
    }
}

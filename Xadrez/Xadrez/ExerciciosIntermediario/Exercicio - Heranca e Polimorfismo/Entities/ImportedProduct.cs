﻿using System;
using System.Globalization;

namespace Xadrez.ExerciciosIntermediario.Exercicio___Heranca_e_Polimorfismo.Entities
{
    class ImportedProduct : Product
    {

        public double CustomsFee { get; private set; }


        public ImportedProduct()
        {

        }

        public ImportedProduct(string name, double price, double customFee) : base (name, price)
        {
            CustomsFee = customFee;
        }

        public override string PriceTag()
        {
            return base.PriceTag() + " (Customs Fee: $" + CustomsFee.ToString(CultureInfo.InvariantCulture) + ")";
        }

    }
}

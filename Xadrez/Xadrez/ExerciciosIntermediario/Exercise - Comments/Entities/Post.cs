﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xadrez.ExerciciosIntermediario.Exercise___Comments.Entities
{
    class Post
    {
        public DateTime Moment { get; private set; }
        public string Title { get; private set; }
        public string Content { get; private set; }
        public int Likes { get; private set; }
        public List<Comment> Comments { get; private set; } = new List<Comment>();
        public Post()
        {
          
        }

        public Post(DateTime moment, string title)
        {
            Moment = moment;
            this.Title = title;
        }

        public Post(DateTime moment, string title, string content, int likes) : this(moment,title)
        {
            this.Content = content;
            this.Likes = likes;
        }

        public void AddComment (Comment comment)
        {
            this.Comments.Add(comment);
        }

        public void RemoveComment(Comment comment)
        {
            this.Comments.Remove(comment);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(this.Title);
            sb.Append(this.Likes);
            sb.Append(" Likes - ");
            sb.AppendLine(Moment.ToString("dd/MM/yyyy HH:mm:ss"));
            sb.AppendLine(this.Content);
            sb.AppendLine("Comments: ");

            foreach (Comment c in Comments)
            {
                sb.AppendLine(c.Text);
            }

            return sb.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xadrez.ExerciciosIntermediario.Exercise___Comments.Entities
{
    class Comment
    {
        public string Text { get; private set; }

        public Comment()
        {

        }

        public Comment (string text)
        {
            this.Text = text;
        }
    }
}

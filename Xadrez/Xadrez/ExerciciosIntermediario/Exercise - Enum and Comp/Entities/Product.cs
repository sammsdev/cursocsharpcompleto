﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xadrez.ExerciciosIntermediario.Exercise___Enum_and_Comp.Entities
{
    class Product
    {
        public string Name { get; private set; }
        public double Price { get; private set; }

        public Product(string name, double price)
        {
            this.Name = name;
            this.Price = price;
        }

        public void AlterPrice(double newPrice)
        {
            this.Price = newPrice;
        }
    }
}

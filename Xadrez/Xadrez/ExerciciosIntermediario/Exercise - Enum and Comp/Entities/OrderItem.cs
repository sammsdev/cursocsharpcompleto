﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xadrez.ExerciciosIntermediario.Exercise___Enum_and_Comp.Entities
{
    class OrderItem
    {
        public int Qtd { get; private set; }
        public double ItemPrice { get; private set; }

        public Product ItemOrder { get; private set; }


        public OrderItem()
        {

        }

        public OrderItem(int qtd, double price, Product item)
        {
            this.Qtd = qtd;
            this.ItemPrice = price;
            this.ItemOrder = new Product(item.Name, item.Price);
        }


        public double SubTotal()
        {
            return Qtd * ItemPrice;
        }
    }

}

﻿namespace Xadrez.ExerciciosIntermediario.Exercise___Enum_and_Comp.Entities.Enum
{
    enum OrderStatus : int
    {
        PENDING_PAYMENT = 0,
        PROCESSING = 1,
        SHIPPED = 2,
        DELIVERED = 3
    };
}

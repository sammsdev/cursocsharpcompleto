﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xadrez.ExerciciosIntermediario.Exercise___Enum_and_Comp.Entities
{
    class Client
    {
        public string Name { get; private set; }
        public string Email { get; private set; }
        public DateTime Date { get; private set; }

        public Client(string name, string email, DateTime date)
        {
            this.Name = name;
            this.Email = email;
            this.Date = date;
        }

        public void AlterEmail(string newEmail)
        {
            this.Email = newEmail;
        }
    }
}

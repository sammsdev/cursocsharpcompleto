﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using Xadrez.ExerciciosIntermediario.Exercise___Enum_and_Comp.Entities.Enum;

namespace Xadrez.ExerciciosIntermediario.Exercise___Enum_and_Comp.Entities
{
    class Order
    {
        public DateTime Moment { get; private set; }
        public OrderStatus Status { get; private set; }
        public List<OrderItem> Items { get; private set; } = new List<OrderItem>();
        public Client OrderClient { get; private set; }


        public Order(DateTime moment, OrderStatus status, Client client)
        {
            this.Moment = moment;
            this.Status = status;
            this.OrderClient = new Client(client.Name, client.Email, client.Date) ;
        }

        public void AddItem(OrderItem item)
        {            
            this.Items.Add(item);
        }

        public void RemoveItem(OrderItem item)
        {
            this.Items.Remove(item);
        }

        public double Total()
        {
            double sum = 0;
            foreach(OrderItem o in Items)
            {
                sum += o.SubTotal();
            }
            return sum;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("\n\nORDER SUMMARY: ");
            sb.Append("Order moment: ");
            sb.AppendLine(this.Moment.ToString());
            sb.Append("Order Status: ");
            sb.AppendLine(this.Status.ToString());
            sb.Append("Client: ");
            sb.Append(OrderClient.Name);
            sb.Append("  ");
            sb.Append(OrderClient.Date);
            sb.Append(" - ");
            sb.AppendLine(OrderClient.Email);
            sb.AppendLine("ORDER ITEMS: ");

            foreach (OrderItem i in Items)
            {
                sb.Append(i.ItemOrder.Name);
                sb.Append(", Quantidade: ");
                sb.Append(i.Qtd);
                sb.Append(", Subtotal: $");
                sb.AppendLine(i.SubTotal().ToString("F2", CultureInfo.InvariantCulture));

            }
            sb.Append("Total price: ");
            sb.AppendLine(Total().ToString("F2", CultureInfo.InvariantCulture));
            return sb.ToString();
        }

    }
}

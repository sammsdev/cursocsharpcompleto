﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xadrez.ExerciciosIntermediario.Exercise___Tax___Abstracts.Entities
{
    class PessoaFisica : Contribuinte
    {

        public double HealthCare { get; private set; }

        public PessoaFisica(string nome, double rendaAnual, double healthCare) : base (nome, rendaAnual)
        {
            HealthCare = healthCare;
        }

        public override double CalculateTax()
        {
            double tempTax = RendaAnual > 20000 ? RendaAnual * 0.25 : RendaAnual * 0.15;
            return (tempTax -= HealthCare > 0 ? HealthCare * 0.5 : tempTax);

        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}

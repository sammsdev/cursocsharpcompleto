﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xadrez.ExerciciosIntermediario.Exercise___Tax___Abstracts.Entities
{
    class PessoaJuridica : Contribuinte
    {
         public int NumeroFuncionarios { get; private set; }

        public PessoaJuridica(string nome, double rendaAnual, int numeroFuncionarios) : base(nome, rendaAnual)
        {
            NumeroFuncionarios = numeroFuncionarios;
        }

        public override double CalculateTax()
        {
            return NumeroFuncionarios > 10 ? RendaAnual * 0.14 : RendaAnual * 0.16;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}

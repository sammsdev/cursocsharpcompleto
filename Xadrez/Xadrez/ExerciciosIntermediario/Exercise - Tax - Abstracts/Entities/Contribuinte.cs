﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xadrez.ExerciciosIntermediario.Exercise___Tax___Abstracts.Entities
{
    abstract class Contribuinte
    {

        public string Nome { get; private set; }
        public double RendaAnual { get; private set; }

        public Contribuinte()
        {

        }

        public Contribuinte(string nome, double rendaAnual)
        {
            Nome = nome;
            RendaAnual = rendaAnual;
        }


        public abstract double CalculateTax();

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Nome);
            sb.Append(": $");
            sb.AppendLine(CalculateTax().ToString());
            return sb.ToString();
        }

    }
}

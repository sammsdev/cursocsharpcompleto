﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xadrez.ExerciciosIntermediario.Exercise___Exception.Exceptions
{
    class AccountException : ApplicationException
    {
        public AccountException(string message) : base(message)
        {

        }
        
    }
}

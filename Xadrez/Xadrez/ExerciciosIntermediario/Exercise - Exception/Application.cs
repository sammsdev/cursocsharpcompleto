﻿using System;
using System.Collections.Generic;
using System.Text;

using Xadrez.ExerciciosIntermediario.Exercise___Exception.Entities;
using Xadrez.ExerciciosIntermediario.Exercise___Exception.Exceptions;

namespace Xadrez.ExerciciosIntermediario.Exercise___Exception
{
    class Application
    {
        static public void Main(string[] args)
        {

            try
            {
                Console.WriteLine("Enter Account Data: ");
                Console.Write("Number: ");
                var tempNumber = int.Parse(Console.ReadLine());
                Console.Write("Holder: ");
                var tempHolder = Console.ReadLine();
                Console.Write("Initial Balance: ");
                var tempBalance = double.Parse(Console.ReadLine());
                Console.Write("Limit: ");
                var tempLmt = double.Parse(Console.ReadLine());
                Account acc = new Account(tempNumber, tempHolder, tempBalance, tempLmt);

                Console.Write("\n\nEnter ammount for withdraw: ");
                var tempAmmout = double.Parse(Console.ReadLine());

                acc.Withdraw(tempAmmout);
                Console.Write("Final Balance: $" + acc.ToString());

            }
            catch (AccountException e)
            {
                Console.Write(e.Message);
            }

            catch (FormatException e)
            {
                Console.Write(e.Message);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }




        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xadrez.ExerciciosIntermediario.Exercise___Exception.Exceptions;

namespace Xadrez.ExerciciosIntermediario.Exercise___Exception.Entities
{
    class Account
    {
        public int Number { get; private set; }
        public string Holder { get; private set; }
        public double Balance { get; private set; }
        public double WithdrawLimit { get; private set; }

        public Account() { }
        public Account(int number, string holder, double balance, double withdrawlimite)
        {
            Number = number;
            Holder = holder;
            Balance = balance;
            WithdrawLimit = withdrawlimite;

        }

        public void Deposit(double ammount)
        {
            Balance += ammount;
        }

        public void Withdraw(double ammount)
        {
            if (ammount > WithdrawLimit)
                throw new AccountException("Ammount exceeds Withdraw Limite");
            if (ammount > Balance)
                throw new AccountException("Not enought balance. Go to work.");
            Balance -= ammount;

        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("_______Conta______");
            sb.Append(Number);
            sb.Append(" ");
            sb.Append(Holder);
            sb.Append(" ");
            sb.Append(Balance.ToString("F2", CultureInfo.InvariantCulture));
            return sb.ToString();
        }
    }
}

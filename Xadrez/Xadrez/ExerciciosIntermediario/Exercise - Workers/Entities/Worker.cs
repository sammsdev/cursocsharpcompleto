﻿using System;
using System.Collections.Generic;
using System.Text;
using Xadrez.ExerciciosIntermediario.Entities.Enums;

namespace Xadrez.ExerciciosIntermediario.Entities
{
    class Worker
    {
        public string Name { get; private set; }
        public WorkerLevel Level { get; private set; }
        public double BaseSalary { get; private set; }

        public Department Department { get; private set; }

        public List<HourContract> Contracts { get; private set; } = new List<HourContract>();

        public Worker(string name, WorkerLevel level, double baseSalary, string department)
        {
            this.Name = name;
            this.Level = level;
            this.BaseSalary = baseSalary;
            this.Department = new Department(department);
        }

        public void AddContract(HourContract hourContract)
        {
            this.Contracts.Add(hourContract);
        }
        public void RemoveContract(HourContract hourContract)
        {
            this.Contracts.Remove(hourContract);
        }

        public double Income(int month, int year) 
        {
            double sum = 0;

            foreach(HourContract c in Contracts)
            {
                if (year == c.Date.Year && c.Date.Month == month)
                    sum += c.TotalValue();
            }
            
            return sum + BaseSalary;
        }

        public override string ToString()
        {
            return "Name: " + this.Name + "\nDepartment: " + this.Department.Name;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xadrez.ExerciciosIntermediario.Entities
{
    class Department
    {
        public string Name { get;  private set; }

        public Department(string name)
        {

            this.Name = name;
        }

    }
}

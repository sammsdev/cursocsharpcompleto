﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using Xadrez.ExerciciosIntermediario.Entities;
using Xadrez.ExerciciosIntermediario.Entities.Enums;

namespace Xadrez.ExerciciosIntermediario
{
    
    class Exercise1_Worker
    {
        /*
         * 
         * 
        static public void Main(string[] args)
        {

            Console.Write("Enter Department's name: ");
            string tempDepartment = Console.ReadLine();

            Console.WriteLine("Enter worker data: ");
            Console.Write("Name: ");
            string tempName = Console.ReadLine();
            Console.Write("Level (Junior/MidLevel/Senior): ");
            WorkerLevel tempLevel = Enum.Parse<WorkerLevel>(Console.ReadLine());
            Console.Write("Base salary: ");
            double tempBase = double.Parse(Console.ReadLine());

            Worker worker = new Worker(tempName, tempLevel, tempBase, tempDepartment);
            ReceiveContracts(worker);

            Console.Write("Enter month and year to calculate income (MM/YYYY): ");
            string monthAndYear = Console.ReadLine();
            double totalIncome = worker.Income(int.Parse(monthAndYear.Substring(0, 2)), int.Parse((monthAndYear.Substring(3))));

            Console.WriteLine(worker.ToString());
            Console.WriteLine("Income for " + monthAndYear + ": $" + totalIncome.ToString("F2"));


        }



        private static void ReceiveContracts(Worker w)
        {
            Console.Write("How many contracts for this worker?: ");
            int qtdContracts = int.Parse(Console.ReadLine());

            for (int i = 0; i < qtdContracts; i++)
            {
                Console.WriteLine("Enter contrract #" + (i + 1) + " data");
                Console.Write("Date (DD/MM/YYYY): ");
                DateTime tempDate = DateTime.Parse(Console.ReadLine());
                Console.Write("Value per hour: ");
                double tempValue = double.Parse(Console.ReadLine());
                Console.Write("Duration (hours): ");
                int tempDuration = int.Parse(Console.ReadLine());
                w.AddContract(new HourContract(tempDate, tempValue, tempDuration));
            }

        }


        */
    }
}

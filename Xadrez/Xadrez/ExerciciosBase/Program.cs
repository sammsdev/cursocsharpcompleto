﻿using System;
using System.Collections.Generic;

namespace Xadrez
{
    class Program
    {
        #region ProgramasBase 
        /*
        static void Main(string[] args)
        {
            #region exercicio -  Estoque
            /* Produto p = new Produto();         
             Console.WriteLine("Entre os dados do produto: ");

             Console.Write("Nome: ");
             p.Nome = Console.ReadLine();
             Console.Write("Preco: ");
             p.Preco = double.Parse(Console.ReadLine());
             Console.Write("Quantidade no estoque: ");
             p.Quantidade = int.Parse(Console.ReadLine());

             Console.WriteLine("Dados do produto: " + p.ToString()); 

            #endregion

            #region Exercicio - Cotacao Dolar - Metodos Estaticos 
            
            Console.Write("Qual é a cotação do dólar? ");
            ConversorMoeda.SetCotacao(double.Parse(Console.ReadLine()));
            Console.Write("Quantos dólares você vai comprar? ");
            double r = ConversorMoeda.ValorPago(double.Parse(Console.ReadLine()));
            Console.Write("Valor a ser pago em reais = $" + r.ToString("F2")); 
            #endregion


            #region Exercicio ContaBancaria
            
            
            Console.Write("Entre o numero da conta: ");
            int n = int.Parse(Console.ReadLine());

            Console.Write("Entre o titular da conta: ");
            string t = Console.ReadLine();
              
            Console.WriteLine("\n\nDados da conta: ");
            ContaBancaria conta = new ContaBancaria(n, t.ToUpper());
            Console.WriteLine(conta.ToString());

            int r = -1;

            while(r != 3)
            {
                Console.Write("\n\nOperacoes: 1 - Deposito, 2 - Saque , 3 - Sair : ");
                r = int.Parse(Console.ReadLine());
                switch (r)
                {
                    case 1:
                        Console.Write("Entre um valor para deposito: ");
                        double dep = double.Parse(Console.ReadLine());
                        conta.RealizarDeposito(dep);
                        break;
                    case 2:
                        Console.Write("Entre um valor para saque: ");
                        double saque = double.Parse(Console.ReadLine());
                        conta.RealizarSaque(saque);
                        break;
                    case 3:
                        Console.Write("Saindo... ");
                        break;
                    default:
                        Console.Write("Opcao invalida");
                        break;
                }

                Console.WriteLine("\n\nDados da conta atualizdo");
                Console.WriteLine(conta.ToString());

            } 



            #endregion


            #region Exercicio FUncionario - Listas 
            
            Console.Write("How many employees will be registered? ");
            int qtd = int.Parse(Console.ReadLine());
            List<Funcionario> employees = new List<Funcionario>();

            for (int i = 0; i < qtd; i++)
            {
                Console.WriteLine("Employee #" + (i + 1));
                Console.Write("ID: ");
                int tempId = int.Parse(Console.ReadLine());
                Console.Write("Name: ");
                string tempName = Console.ReadLine();
                Console.Write("Salary: ");
                double tempSal = double.Parse(Console.ReadLine());
                employees.Add(new Funcionario(tempId, tempName, tempSal));
                Console.Write("\n\n");
            }
           

            Console.WriteLine("Enter the employee ID that will hava salary increase: ");
            int id = int.Parse(Console.ReadLine());
            Console.Write("Enter the percentage: ");
            double perc = double.Parse(Console.ReadLine());
            int index = employees.FindIndex(x => x.Id == id);

            if (index != -1)
                employees[index].increaseSalary(perc);
            else
                Console.Write("This id does not exist!");


            Console.WriteLine("Updated list of employees:");

            foreach (Funcionario f in employees)
                Console.WriteLine(f.ToString());

            Console.ReadLine();
            
            #endregion

            #region Exercicio Matriz
            
            Console.Write("Digite o tamanho da Matriz: ");
            int n = int.Parse(Console.ReadLine());
            int[,] matriz = new int[n, n];
            Console.WriteLine("-----Preenchendo a matriz---- ");
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                {
                    Console.WriteLine("posicao: " + i + ", " + j);
                    matriz[i, j] = int.Parse(Console.ReadLine());
                }

            Console.WriteLine("\n-----Diagonal---- ");
            for (int i = 0; i < n; i++)
                Console.Write(matriz[i, i] + "\n");


            Console.WriteLine("\n-----Negativos---- ");
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                {
                    if(matriz[i,j]< 0)
                        Console.Write(matriz[i, j] + "\n");
                   
                }


            
            #endregion


        }
        */
        #endregion
    }
}

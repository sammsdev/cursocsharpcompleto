﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xadrez
{
    static class ConversorMoeda
    {
        private static double Iof = 1.06;
        public static double Cotacao { get; private set; }

        public static void SetCotacao(double c)
        {
            Cotacao = c;
        }

        public static double ValorPago (double qtd)
        {
            return qtd * Cotacao * Iof;
        }

    }
}

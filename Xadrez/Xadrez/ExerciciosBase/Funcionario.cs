﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Xadrez
{
    class Funcionario
    {
        public int Id { get; private set; }
        public string Name { get; private set; }

        public double Salary { get; private set; }


        public Funcionario(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public Funcionario(int id, string name, double sal) : this( id, name)
        {
            Salary = sal;
        }

        public void increaseSalary(double percentage)
        {            
            Salary += Salary * percentage / 100;            
        }

        public override string ToString()
        {
            return Id + ", " + Name + ", R$ " + Salary.ToString("F2", CultureInfo.InvariantCulture);
        }
    }
}

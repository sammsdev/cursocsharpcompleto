﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xadrez
{
    class Produto
    {
        public string Nome;
        public double Preco;
        public int Quantidade;


        public double ValorTotalEmEstoque()
        {
            return Preco * Quantidade;
        }

        public override string ToString()
        {
            return Nome + " , Preco: $" + Preco.ToString("F2") + " , Quantidade: " + Quantidade + " , Total em Estoque: $" + ValorTotalEmEstoque();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xadrez
{
    class ContaBancaria
    {
        public int Numero { get; private set; }
        public string Titular { get;  set; }

        public double Saldo { get; private set; }

        
        public ContaBancaria()
        {

        }
        public ContaBancaria(int numero, string titular)
        {
            this.Numero = numero;
            this.Titular = titular;
            this.Saldo = 0;
        }

        public ContaBancaria(int numero, string titular, double saldo) : this(numero, titular)
        {
            this.Saldo = saldo;
        }

        public void RealizarDeposito(double saldo)
        {
            if(saldo > 0)
                this.Saldo += saldo;
            
        }
        public void RealizarSaque(double saque)
        {
            this.Saldo -= saque;
        }


        public override string ToString()
        {
            return "Conta " + Numero.ToString() + ", Titular: " + Titular.ToString() + ", Saldo: " + Saldo.ToString();
        }
    }
}
